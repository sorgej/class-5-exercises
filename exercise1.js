// JSCRIPT 200 Class 5 Homework 

// Class Exercise 1 
// Traversing the DOM
// 1)
let main = body.firstChild;  // goes to <main>

// 2)
let body = ul.parentNode
              .parentNode; // goes to <body>

// 3)
let li = p.previousSibling.lastChild;  // goes to 3rd <li>
