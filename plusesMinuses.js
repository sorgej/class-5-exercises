// JSCRIPT 200 Class 5 Homework 


// Class Exercise 2
// Product Crud

// 1
// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let body = document.body;
let main = body.getElementsByTagName('main')[0];

let plusEl = document.getElementById('plus');
let minusEl = document.getElementById('minus');
let countEl = document.getElementById('count');
let counter = 0;

countEl.innerHTML = 0;

plusEl.addEventListener('click', function(e) {
    counter++;
    countEl.innerHTML = counter;
})

minusEl.addEventListener('click', function(e) {
    counter--;
    countEl.innerHTML = counter;
})