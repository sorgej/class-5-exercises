// JSCRIPT 200 Class 5 Homework 


// Class Exercise 2
// Product Crud

// 1
// Create a new <a> element containing the text "Buy Now!" 
// with an id of "cta" after the last <p>
let body = document.body;
let main = body.getElementsByTagName('main')[0];

// let lastP = main.getElementsByTagName('p')[0];
const cta = document.createElement('a');
const ctaText = document.createTextNode('Buy Now!');
cta.appendChild(ctaText);
cta.setAttribute('id', 'cta');

main.appendChild(cta);


// 2
// Access (read) the data-color zattribute of the <img>,
// log to the console
let imgEl = main.getElementsByTagName('img')[0];
console.log(imgEl.dataset.color); 

// 3
// Update the third <li> item ("Turbocharged"), 
// set the class name to "highlight"
let liEl = main.getElementsByTagName('li')[2];
liEl.setAttribute('class', 'highlight');

// 4
// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
let pEl = main.getElementsByTagName('p')[0];
let removeEl = main.removeChild(pEl);