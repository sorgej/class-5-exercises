let body = document.body;
let main = body.getElementsByTagName('main')[0];


    // const checkmark =
    //   this.getElementsByTagName('span')[0];
//     checkmark.innerHTML =
//   checkmark.innerHTML === '□' ? '✓' :
//   '□'; 
    //const liEls = this.getElementsByTagName('li');


// If an li element is clicked, toggle the class "done" on the <li>
//let liEl = document.getElementByTagName('li');

const toggleToDo = function(e) {
    this.parentElement.classList.toggle('done');
}
  const liToDos =
    document.querySelectorAll('span');
  for (let i = 0; i < liToDos.length; i++) {
        console.log(liToDos[i].firstChild);
        liToDos[i].addEventListener('click', toggleToDo);
  }


// If a delete link is clicked, delete the li element / remove from the DOM
const deleteToDo = function(e) {
    this.parentNode.remove('li');
}

const deleteToDos =
document.querySelectorAll('a.delete');
for (let i = 0; i < deleteToDos.length; i++) {
    console.log(deleteToDos[i].firstChild);
    deleteToDos[i].addEventListener('click', deleteToDo);
}


// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>
const moveToDo = function(e) {
    if (this.parentElement.parentElement.className === 'today-list')  {
        const laterUlEl = main.getElementsByClassName('later-list')[0];
        this.parentElement.remove;
        //this.parentNo.remove('li');
        this.innerHTML = ('Move to Today');
        laterUlEl.appendChild(this.parentElement);
        this.classList.remove('toLater');
        this.classList.add('toToday');
    } else {
        const todayUlEl = main.getElementsByClassName('today-list')[0];
        this.parentElement.remove;
        //this.parentNo.remove('li');
        this.innerHTML = ('Move to Later');
        todayUlEl.appendChild(this.parentElement);
        this.classList.remove('toToday');
        this.classList.add('toLater');
    }
}

const moveToDos =
document.querySelectorAll('a.move');
for (let i = 0; i < moveToDos.length; i++) {
    console.log(moveToDos[i]);
    moveToDos[i].addEventListener('click', moveToDo);
}


// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!  
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
    e.preventDefault();

    const input = this.parentNode.getElementsByTagName('input')[0];
    if (input.value === '') {
        return; 
    }
    const text = input.value; // use this text to create a new <li>
    // Finish function here
    const addLiEl = document.createElement('li');
    const addSpanEl = document.createElement('span');
    const addTextEl = document.createTextNode(text);
    addLiEl.appendChild(addSpanEl);

    addSpanEl.appendChild(addTextEl);

    const addMoveButton = document.createElement('a');
    const addDeleteButton = document.createElement('a');

    if (this.parentElement.previousElementSibling.className === 'today-list')  {
        const todayUlEl = main.getElementsByClassName('today-list')[0];
        todayUlEl.appendChild(addLiEl);
        addMoveButton.innerHTML = 'Move to Later';
        addMoveButton.classList.add('toLater');
    } else {
        const laterUlEl = main.getElementsByClassName('later-list')[0];
        laterUlEl.appendChild(addLiEl);
        addMoveButton.innerHTML = 'Move to Today';
        addMoveButton.classList.add('toToday');
    };

    addDeleteButton.innerHTML = 'Delete';
    addDeleteButton.classList.add('delete');
    addMoveButton.classList.add('move');
    addLiEl.appendChild(addMoveButton);
    addLiEl.appendChild(addDeleteButton);


    // Add Event Listeners or new item. Wish I knew how to append these elements to current even listener lists
	addLiEl.firstChild.addEventListener('click', toggleToDo);
    //liToDos.append(addLiEl.firstChild);

	addLiEl.querySelectorAll('a.delete')[0].addEventListener('click', deleteToDo);

	addLiEl.getElementsByClassName('move')[0].addEventListener('click', moveToDo);
    
    input.value = '';
}
  
  // Add this as a listener to the two Add links
const addToDos =
document.querySelectorAll('a.add-item');
for (let i = 0; i < addToDos.length; i++) {
    console.log(addToDos[i]);
    addToDos[i].addEventListener('click', addListItem);
}
